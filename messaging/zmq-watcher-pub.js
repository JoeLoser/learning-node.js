/*
  * Important change from old networking: only one call to fs.watch()
  * Originally invoked watch() once for each connected client
  * Here, we just use one fs watcher which invokes the publisher's
    send method
*/

'use strict';
const
fs = require('fs'),
zmq = require('zmq'),

// create publisher endpoint
publisher = zmq.socket('pub'),

filename = process.argv[2];

fs.watchFile(filename, function() {
  // send message to subscribers
  publisher.send(JSON.stringify({
    type: 'changed',
    file: filename,
    timestamp: Date.now()
  }));
});

// listen on TCP port 5432
publisher.bind('tcp://*:5432', function(err) {
  console.log('Listening for zmq subscribers...');
});