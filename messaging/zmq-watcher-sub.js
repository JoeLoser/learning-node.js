"use strict";

var zmq = require('zmq');

// create subscriber endpoint
var subscriber = zmq.socket('sub');

// subscribe to all messages
// If wanted to just receive certain messages, could provide string
// that acts a prefix filter. 
// Must call subscribe() before able to receive any messages 
subscriber.subscribe("");

// subscriber object emits a message event whenever it receives one
// from the publisher, so use subscriber.on() to listen for them
// handle messages from publisher
subscriber.on("message", function(data) {
  let
    message = JSON.parse(data),
    date = new Date(message.timestamp);
  console.log("File '" + message.file + "' changed at " + data);
});

// connect to publisher
subscriber.connect("tcp://localhost:5432");
