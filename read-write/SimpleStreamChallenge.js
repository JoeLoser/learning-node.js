// var fs = require('fs'),
//     readline = require('readline');

// var rd = readline.createInterface({
//     input: fs.createReadStream('./input.txt'),
//     output: process.stdout,
//     terminal: false
// });

// rd.on('line', function(line) {
//     console.log(line);
// });
 
var fs = require('fs'),
    stream = fs.createReadStream(process.argv[2]);

stream.on('data', function(chunk) {
  process.stdout.write(chunk);
});

stream.on('error', function(err) {
  process.stderr.write("ERROR: " + err.message + "\n");
});

// //Ref: https://github.com/nickewing/line-reader
// var lineReader = require('line-reader');

// lineReader.open('input.txt', function(reader, err) {
//   if(err) {
//     throw Error("Could not open the file.");
//   }
//   if (reader.hasNextLine()) {
//     reader.nextLine(function(line) {
//       console.log(line);
//     });
//   }
// });