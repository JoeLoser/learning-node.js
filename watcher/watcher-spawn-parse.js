"use strict";
const
  fs = require('fs'),
  spawn = require('child_process').spawn,
  filename = process.argv[2];

if (!filename) {
  throw Error("A file to watch must be specified");
}

fs.watch(filename, function() { 
  let 
    ls = spawn('ls', ['-lh', filename]);
    var output = '';

  //on() method adds a listener for the specified event type
  ls.stdout.on('data', function(chunk) {
    output += chunk.toString();
  });

  ls.on('close', function() {
    let parts = output.split(/\s+/); //split on sequences of one or more whitespace char
    console.dir(parts[0], parts[4], parts[8]); //permissions, size, and file name
  });
});

console.log("Now watching " + filename + " for changes...");
