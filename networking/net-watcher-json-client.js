"use strict";
const
  net = require('net'),
  //Create a client connection to port 5432 and then wait for data
  //Client object is a socket
  client = net.connect({port: 5432});
 
 //Whenever a data event happens, our cb function takes the incoming buffer object, p
 //Parses the JSON message, and then logs an appropriate message to the console
  client.on('data', function(data) {
    let message = JSON.parse(data);
    if (message.type === 'watching') {
      console.log("Now watching: " + message.file);
    } 
    else if (message.type === 'changed') {
      let date = new Date(message.timestamp);
      console.log("File '" + message.file + "' changed at " + date);
    } 
    else {
      throw Error("Unrecognized message type: " + message.type);
    }
  });
